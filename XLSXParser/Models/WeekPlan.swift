//
//  WeekPlan.swift
//  XLSXParser
//
//  Created by Jose Antonio Mendoza on 23/1/23.
//

import Foundation

struct WeekPlan {
    let id = UUID()
    let agent: Int
    let week: [Shift]
}

struct Shift {
    let id = UUID()
    let name: String
    let date: Date
}
