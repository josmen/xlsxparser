//
//  main.swift
//  XLSXParser
//
//  Created by Jose Antonio Mendoza on 19/1/23.
//

import ArgumentParser
import CoreXLSX
import Foundation

enum SheetName: String {
    case fecha = "FECHA"
    case maq = "G_BENIDORM"
    case usi = "USIS BENIDORM"
}

enum AgentCF: Int, CaseIterable {
    case jose = 2076
    case itziar = 1508
}

@main
struct App: ParsableCommand {
    @Argument(help: "The filename you want to process.")
    var filepath: String
    
    static var configuration: CommandConfiguration {
        CommandConfiguration(commandName: "xlsxparser", abstract: "Extracts work week shifts from excel file.")
    }
    
    func run() {
        let file = getFile()
        
        // clean terminal window
        print("\u{001B}[2J")
        
        let monday = getDate(from: file)
        let sunday = Calendar.current.date(byAdding: .day, value: 6, to: monday)!
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        print("📅 Week from \(formatter.string(from: monday)) to \(formatter.string(from: sunday))\n")
        
        var weekPlan: [WeekPlan] = []
        for agent in AgentCF.allCases {
            print("🧑🏻 Agent: \(agent.rawValue)")
            let row = getAgentRow(of: agent, from: file)
            var week: [Shift] = []
            let rowShifts = Array(row[2...])
            for i in 0...6 {
                let shift = Shift(name: rowShifts[i], date: Calendar.current.date(byAdding: .day, value: i, to: monday) ?? Date())
                print("\(formatter.string(from: shift.date)) -> \(shift.name)")
                week.append(shift)
            }
            let agentWeekPlan = WeekPlan(agent: agent.rawValue, week: week)
            weekPlan.append(agentWeekPlan)
            print()
        }
    }
    
    private func getFile() -> XLSXFile {
        guard let file = XLSXFile(filepath: filepath) else {
            fatalError("XLSX file at \(filepath) is corrupted or does not exist")
        }
        return file
    }
    
    private func getWorksheet(from file: XLSXFile, sheetName: SheetName) throws -> Worksheet? {
        do {
            let workbook = try file.parseWorkbooks()
            for (name, path) in try file.parseWorksheetPathsAndNames(workbook: workbook.first!) {
                guard name == sheetName.rawValue else { continue }
                let worksheet = try file.parseWorksheet(at: path)
                return worksheet
            }
        } catch {
            print("❌ Error: \(error)")
        }
        return nil
    }
    
    private func getDate(from file: XLSXFile) -> Date {
        guard let worksheet = try? getWorksheet(from: file, sheetName: .fecha) else {
            return .now
        }
        let columnCDates = worksheet.cells(atColumns: [ColumnReference("C")!])
            .compactMap { $0.dateValue }
        return columnCDates.first ?? .now
    }
    
    private func getAgentRow(of agent: AgentCF, from file: XLSXFile) -> [String] {
        guard let worksheet = try? getWorksheet(from: file, sheetName: agent == .itziar ? .usi : .maq) else {
            print("No se ha encontrado nada de nada")
            return []
        }
        
        do {
            if let sharedStrings = try file.parseSharedStrings() {
                let rows = worksheet.data?.rows.filter { row in
                    if !row.cells.isEmpty,
                       let firstCellValue = row.cells.first?.stringValue(sharedStrings),
                       firstCellValue.contains(String(agent.rawValue)) {
                        return firstCellValue.isNumeric
                    }
                    return false
                }
                
                guard rows?.count == 1 else {
                    print("Hay más de fila que comple con agente = \(agent.rawValue)")
                    return []
                }
                
                let cells = rows![0].cells.filter { $0.value?.isEmpty == false }
                var cellsValues: [String] = []
                cells.forEach { cell in
                    if let value = cell.stringValue(sharedStrings) {
                        if value.isNumeric {
                            cellsValues.append(String(Int(Double(value)!)))
                        } else {
                            cellsValues.append(value)
                        }
                    }
                }
                return cellsValues
            }
            return []
        } catch {
            print("❌ Error: \(error)")
            return []
        }
    }
}
